package com.twuc.webApp.yourTurn;

import com.twuc.webApp.WithoutDependency;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class IoCTest {
    @Test
    void should_get_bean_with_component_annotation() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        Object bean = context.getBean(WithoutDependency.class);

        assertNotNull(bean);
    }

    private AnnotationConfigApplicationContext normalContext() {
        return new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
    }

    @Test
    void should_get_bean_with_component_has_dependency_annotation() {
        WithDependency bean = normalContext().getBean(WithDependency.class);

        assertNotNull(bean.dependent);
    }

    @Test
    void should_not_get_out_of_scope_bean() {
        assertThrows(RuntimeException.class, () -> normalContext().getBean(WithoutDependency.class));
    }

    @Test
    void should_get_bean_that_implements_interface_by_interface_class() {
        Interface bean = normalContext().getBean(Interface.class);

        assertNotNull(bean);
    }

//    @Test
//    void should_get_?() {
//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
//
//        SimpleInterface bean = context.getBean(SimpleInterface.class);
//
//        assertNotNull(bean);
//    }

    @Test
    void should_get_bean_by_multiple_constructor() {
        MultipleConstructor bean = normalContext().getBean(MultipleConstructor.class);

        assertNotNull(bean.getDependent());
        assertNull(bean.getName());
    }

    @Test
    void should_post_post_constructor() {
        WithAutowiredMethod bean = normalContext().getBean(WithAutowiredMethod.class);

        assertEquals("B", bean.getOrder());
        assertNull(bean.getDependent());
        assertNotNull(bean.getAnotherDependent());
    }

    @Test
    void should_get_interface_with_multiple_implements() {
        Map<String, InterfaceWithMultipleImpls> beans = normalContext().getBeansOfType(InterfaceWithMultipleImpls.class);

        assertEquals(3, beans.size());
        System.out.println(beans.toString());
    }
}

package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
class WithDependency {
    public Dependent dependent;

    public WithDependency(Dependent dependent) {
        this.dependent = dependent;
    }
}

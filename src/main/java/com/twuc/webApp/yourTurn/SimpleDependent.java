package com.twuc.webApp.yourTurn;

public class SimpleDependent {
    private String name;

    public SimpleDependent() { }

    public SimpleDependent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

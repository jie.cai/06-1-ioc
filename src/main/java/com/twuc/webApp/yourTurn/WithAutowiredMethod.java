package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithAutowiredMethod {
    private Dependent dependent;
    private AnotherDependent anotherDependent;
    private String order = "";

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public Dependent getDependent() {
        return dependent;
    }

    public void setDependent(Dependent dependent) {
        this.dependent = dependent;
    }

    public AnotherDependent getAnotherDependent() {
        return anotherDependent;
    }

    public void setAnotherDependent(AnotherDependent anotherDependent) {
        this.anotherDependent = anotherDependent;
    }

    public WithAutowiredMethod() {}

    public WithAutowiredMethod(Dependent dependent) {
        this.dependent = dependent;
        order += "A";
    }

    @Autowired
    public void initialize(AnotherDependent anotherDependent) {
        this.anotherDependent = anotherDependent;
        order += "B";
    }
}

package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    private String name;
    private Dependent dependent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Dependent getDependent() {
        return dependent;
    }

    public void setDependent(Dependent dependent) {
        this.dependent = dependent;
    }

    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
    }

    public MultipleConstructor(String name) {
        this.name = name;
    }
}

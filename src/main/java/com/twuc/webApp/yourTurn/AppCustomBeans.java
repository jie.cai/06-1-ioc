package com.twuc.webApp.yourTurn;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppCustomBeans {
    @Bean
    public SimpleDependent simpleDependent() {
        return new SimpleDependent("O_o");
    }
}
